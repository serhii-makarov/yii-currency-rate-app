# Yii Rate App

## Installation

### Requirements

* Docker

#### Steps to install

#####Clone repo
```bash
git clone git@bitbucket.org:serhii-makarov/yii-currency-rate-app.git
```
##### Build docker
```bash
docker-compose build
```
##### Run containers
```bash
docker-compose up -d
```
...Wait about 2-3mins until containers starting

Go to http://localhost:8000
