<?php

use omnilight\scheduling\Schedule;

/**
 * @var Schedule $schedule
 */

$schedule->command('currency-actuator/actuate')->everyTenMinutes();