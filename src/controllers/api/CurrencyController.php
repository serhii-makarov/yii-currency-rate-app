<?php

namespace app\controllers\api;

use app\models\Currency;
use Yii;
use yii\db\ActiveQuery;
use yii\rest\Controller;
use yii\web\Response;

class CurrencyController extends Controller
{
    /**
     * @return Response
     */
    public function actionFilter()
    {
        $filters = Yii::$app->request->get();

        $currencies = Currency::find()->with('currencyStats');

        if ($filters['dateFrom']) {
            $currencies->joinWith(['currencyStats' => function (ActiveQuery $query) use ($filters) {
                $query->andWhere(['>=', 'currency_stat.at', $filters['dateFrom'].' 00:00:00']);
            }]);
        }

        if ($filters['dateTo']) {
            $currencies->joinWith(['currencyStats' => function (ActiveQuery $query) use ($filters) {
                $query->andWhere(['<=', 'currency_stat.at', $filters['dateTo'].' 23:59:59']);
            }]);
        }

        if ($filters['currencies']) {
            $currencies->where(['in', 'code', $filters['currencies']]);
        }

        return $this->asJson(
            $currencies->asArray()->all()
        );
    }

}
