#!/bin/bash

set -e

run=${RUN_SCHEDULER:-app}

if [ "$run" = 1 ]; then

    echo "Running Scheduler..."
    while [ true ]
    do
        ./yii schedule/run --schedule-file ./schedule/scheduller.php
        sleep 60
    done
fi
