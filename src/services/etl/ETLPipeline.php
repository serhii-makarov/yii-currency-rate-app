<?php

namespace app\services\etl;

use app\services\etl\contracts\ETLExtractor;
use app\services\etl\contracts\ETLLoader;
use app\services\etl\contracts\ETLTransformer;

abstract class ETLPipeline implements contracts\ETLPipeline
{
    /**
     * @var ETLLoader
     */
    protected $loader;
    /**
     * @var ETLExtractor
     */
    protected $extractor;
    /**
     * @var ETLTransformer
     */
    protected $transformer;

    public $options = [];

    /**
     * @param ETLExtractor $extractor
     * @param ETLTransformer $transformer
     * @param ETLLoader $loader
     * @return bool
     */
    public function run(
        ETLExtractor $extractor,
        ETLTransformer $transformer,
        ETLLoader $loader
    ): bool
    {
        $records = $extractor->extract();
        $transformed = $transformer->transform($records);

        $loader->load($transformed);

        return true;
    }
}