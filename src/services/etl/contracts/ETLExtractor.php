<?php

namespace app\services\etl\contracts;

interface ETLExtractor
{
    public function __construct($options = []);

    /**
     * @return array
     */
    public function extract(): array;
}