<?php

namespace app\services\etl\contracts;

interface ETLTransformer
{
    public function transform(array $records): array;
}