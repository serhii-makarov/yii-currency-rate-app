<?php

namespace app\services\etl\contracts;

interface ETLLoader
{
    /**
     * @param array $transformed
     * @return bool
     */
    public function load(array $transformed): bool;
}