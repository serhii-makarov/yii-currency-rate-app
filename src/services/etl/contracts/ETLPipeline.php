<?php

namespace app\services\etl\contracts;

interface ETLPipeline
{
    /**
     * @param array $options
     * @return mixed
     */
    public function load($options = []);

    /**
     * @param ETLExtractor $extractor
     * @param ETLTransformer $transformer
     * @param ETLLoader $loader
     * @return mixed
     */
    public function run(
        ETLExtractor $extractor,
        ETLTransformer $transformer,
        ETLLoader $loader
    );
}