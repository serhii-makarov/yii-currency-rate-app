<?php

namespace app\services\currency;

use app\services\etl\ETLPipeline;
use app\services\etl\exceptions\ETLException;

class CurrencyActuatePipeline extends ETLPipeline
{
    /**
     * @inheritDoc
     * @throws ETLException
     */
    public function load($options = [])
    {
        $this->options = $options;
        $this->extractor = new CurrencyExtractor($options);
        $this->loader = new CurrencyLoader();
        $this->transformer = new CurrencyTransformer();

        $this->run(
            $this->extractor,
            $this->transformer,
            $this->loader
        );
    }
}