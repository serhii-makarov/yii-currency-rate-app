<?php

namespace app\services\currency;

use app\services\etl\contracts\ETLExtractor;
use app\services\etl\exceptions\ETLException;
use GuzzleHttp\Client;
use SimpleXMLElement;

class CurrencyExtractor implements ETLExtractor
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var array
     */
    private $options;

    /**
     * CurrencyExtractor constructor.
     * @param array $options
     * @throws ETLException
     */
    public function __construct($options = [])
    {
        if (!$options['url']) {
            throw new ETLException('No URL Provided');
        }

        $this->options = $options;

        $this->client = new Client([
            'base_uri' => $this->options['url'],
            'timeout'  => 2.0,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function extract(): array
    {
        $response = $this->client->get('scripts/XML_daily.asp', [
            'headers' => [
                'Accept' => 'application/xml'
            ]
        ]);

        $body = $response->getBody()->getContents();

        $records = new SimpleXMLElement($body);

        $result = [];

        foreach ($records->Valute as $element) {
            $result[] = $element;
        }

        return $result;
    }
}