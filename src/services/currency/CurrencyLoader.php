<?php

namespace app\services\currency;

use app\models\Currency;
use app\models\CurrencyStat;
use app\services\etl\contracts\ETLLoader;

class CurrencyLoader implements ETLLoader
{

    /**
     * @inheritDoc
     */
    public function load(array $transformed): bool
    {
        foreach ($transformed as $record) {
            if (empty($currency = Currency::findOne(['code' => $record['code']]))) {
                $currency = new Currency();
                $currency->code = $record['code'];
            }

            $currency->name = $record['name'];
            $currency->save();

            if (!empty($lastCurrencyStat = CurrencyStat::find()->limit(1)->orderBy('id DESC')->one())) {
                if ($lastCurrencyStat->value != $record['value']) {
                    $currencyStat = new CurrencyStat();

                    $currencyStat->currency_id = $currency->id;
                    $currencyStat->value = $record['value'];
                    $currencyStat->at = $record['at'];

                    $currencyStat->save();
                }
            }
        }
        return true;
    }
}