<?php

namespace app\services\currency;

use app\services\etl\contracts\ETLTransformer;

class CurrencyTransformer implements ETLTransformer
{

    public function transform(array $records): array
    {
        $result = [];

        foreach ($records as $record) {
            $result[] = [
                'code' => (string) $record->CharCode,
                'name' => (string) $record->Name,
                'value' => floatval($record->Value) / floatval($record->Nominal),
                'at' => (new \DateTime())->format('Y-m-d H:i:s')
            ];
        }
        return $result;
    }
}