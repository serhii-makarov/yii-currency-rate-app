<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency_stat}}`.
 */
class m200314_120957_CreateCurrencyStatTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency_stat}}', [
            'id' => $this->primaryKey(),
            'currency_id' => $this->integer(),
            'value' => $this->decimal(8, 4),
            'at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex(
            'idx-currency-currency_id',
            'currency_stat',
            'currency_id'
        );

        $this->addForeignKey(
            'fk-currency-currency_id',
            'currency_stat',
            'currency_id',
            'currency',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-currency-currency_id',
            'currency_stat'
        );

        $this->dropIndex(
            'idx-currency-currency_id',
            'currency_stat'
        );

        $this->dropTable('{{%currency_stat}}');
    }
}
