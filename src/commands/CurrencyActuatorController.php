<?php

namespace app\commands;

use app\services\currency\CurrencyActuatePipeline;
use app\services\etl\exceptions\ETLException;
use yii\console\Controller;

class CurrencyActuatorController extends Controller
{
    /**
     * @throws ETLException
     */
    public function actionActuate()
    {
        (new CurrencyActuatePipeline)->load([
            'url' => 'http://www.cbr.ru/scripts'
        ]);
    }

}
